<?php

class Orders extends Database{
    
    public function __construct(){}


    public static function add($query = []){
  
      $sql='insert into orders()values(';
    
     /* foreach($query as $val){
           $sql.='?,';
      }*/
    
      foreach($query as $val){
         if(is_string($val))
         $sql.= '"'.$val.'",';
         elseif(is_integer($val))
         $sql.= $val.',';
         elseif(empty($val))
         $sql.='"",';
    }

      $sql=rtrim($sql,',');
      $sql.=')';
    
      if(self::$dbObject->exec($sql)>0)
         return true;
      else
         return false;
      //echo $sql;
     /*$res = self::$dbObject->prepare($sql);
      
     if($res->execute($query))
         return true;
      else
         return $res->errorCode()[2];*/
         //print_r($query);
    
    }


    public static function link($query = []){
  
      $sql='insert into state_access()values(';
    
    
      foreach($query as $val){
         if(is_string($val))
         $sql.= "'".$val."',";
         elseif(is_integer($val))
         $sql.= $val.',';
         elseif(empty($val))
         $sql.='"",';
    }

      $sql=rtrim($sql,',');
      $sql.=')';
    
      //echo $sql;
     if(self::$dbObject->exec($sql)>0)
         return true;
      else
         return false;
    
    }

    public static function commitState($uid){

      //self::$dbObject->beginTransaction();

      if(!empty($uid)){
         $q = self::$dbObject->query("update users set is_stated = 1 where u_id =".$uid);
         if($q->rowCount()>0){
           // self::$dbObject->commit();
            echo 'Updated successfully';
         }else{
            var_dump($q->errorCode());
            //self::$dbObject->rollback();
         }
      }else{
         echo 'Set a valid ID';
      }
     
    }



    public static function getStates($uid){
       if(!empty($uid))
          return self::$dbObject->query("select s_states from state_access where u_id =".$uid)->fetch(PDO::FETCH_ASSOC);   
    }


     public function updateOrder($data=[],$order_no){
      $sql = "Update orders set fin_attach = '".$data[0]. "',fin_user = '".$data[1]."',fin_isPaid = '".$data[2]."',o_state=2 Where o_id=".$order_no;

      //echo $sql;
      $res = self::$dbObject->query($sql);

      if($res->rowCount()>0)
         echo 'Procced successfully.' ;
      else
         print_r(['message'=>$sql  ,'status'=>false]);
   }

   public function modifyOrder($data=[],$order_no){
      $sql = "Update orders set o_id = '".$data[0]. "',customer_name = '".$data[1]."',customer_mobile = '".$data[2]."',custommer_address='".$data[3]."',p_name = '".$data[4]."',sku = '".$data[5]."',ship_type = '".$data[6]."',delivery_method = '".$data[7]."',pay_method = '".$data[8]."',city = '".$data[9]."',p_cat = '".$data[10]."',note = '".$data[11]."',hafa = '".$data[12]."',building = '".$data[13]."',apart = '".$data[14]."' Where o_id='".$order_no."'";

      //echo $sql;
      $res = self::$dbObject->query($sql);

      if($res->rowCount()>0)
         echo 'Procced successfully.' ;
      else
         print_r(['message'=>$sql  ,'status'=>false]);
   }

   public function searchByCS($order_id){
      return (!empty(self::$dbObject))?self::$dbObject->query("select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_id LIKE '%".$order_id."%'")->fetchall(PDO::FETCH_ASSOC):null; 
    }

    public function isSKUExisted($sku){

      if(!empty(trim($sku))){
         $res =self::$dbObject->query("select count(*) from products where p_sku='".$sku."'");
         if($res->fetchColumn()>0){
            return '1';
         }
      }else{
         return '0';
      }
    }

    public function isOrderExisted($o_id){
      if(!empty(trim($o_id))){
         $res =self::$dbObject->query("select count(*) from orders where o_id='".$o_id."'");
         if($res->fetchColumn()>0){
            return '1';
         }
      }else{
         return '0';
      }
    }


    public function filterByState($state_id){
      return (!empty(self::$dbObject))?self::$dbObject->query("select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_state =".$state_id)->fetchall(PDO::FETCH_ASSOC):null; 
    }


    public function changeState($orders,$state){

      $alert=false;

      foreach ($orders as $val) {
      
         $sql = "Update orders set o_state = ".$state. " Where o_id=".$val;
            
         
            //echo $sql;
            $res = self::$dbObject->query($sql);

            if($res->rowCount()>0){
               $alert = true ;
            }else{
               $alert = false;
            }
               //echo $val.'  '.$state;

      }

      if($alert){
         echo 'Successfully proced';
      }else{
         echo 'Faild'; 
      }
      
      return $this;
    }


    public function log(){

    }


    public function delProduct($order_id,$sku){

      $q = self::$dbObject->prepare("delete from products where o_id = ? and p_sku = ?");

      $done = $q->execute(array($order_id,$sku));
      if($done){
         echo 'Deleted successfully';
      }else{
         echo 'Something goes wrong';
      }

    }


    public function updateProduct($sku,$title,$cate,$qty,$desc,$o_id){

      $q = self::$dbObject->prepare("update products set p_sku = ?, p_title = ?, p_cate = ?, p_qty = ?, p_desc = ? where p_sku = ? and o_id = ?");

      $done = $q->execute([$sku, $title, $cate, $qty, $desc, $sku, $o_id]);
      if($done){
         echo 'Updated successfully';
      }else{
         echo 'Something goes wrong';
      }

    }


    public function addProducts($products,$order_id){


      $sql = "insert into products()values";

      foreach ($products as $product) {
             $sql.="('".$order_id."','".$product[0]."','".$product[1]."',".$product[2].",".$product[3].",'','".date('Y-m-d H:i:s')."'),";
      }

      $sql = rtrim($sql,',');
      $res = self::$dbObject->query($sql);

      if($res->rowCount()>0){
         return 1;
      }else{
         return 0;
      }
      

      //echo $sql;
    }


   public function searchByFin($order_id){
      return (!empty(self::$dbObject))?self::$dbObject->query("select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_state=1 and fin_isPaid=0 and fin_attach ='' and o_id LIKE '%".$order_id."%'")->fetchall(PDO::FETCH_ASSOC):null; 
    }

    public function searchByStore($order_id){
      return (!empty(self::$dbObject))?self::$dbObject->query("select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_state=2 and o_id LIKE '%".$order_id."%'")->fetchall(PDO::FETCH_ASSOC):null; 
    }

    public function updateOrderStateForCS($data=[],$order_no){
      $sql = "Update orders set o_state = ".$data[0]. ",req_cancel_reason=".$data[1]." Where o_id=".$order_no;

      //echo $sql;
      $res = self::$dbObject->query($sql);

      if($res->rowCount()>0)
         echo 'Confirmed' ;
      else
         print_r(['message'=>$sql  ,'status'=>false]);
    }

   /*public function updateOrderForStore($data=[],$order_no){
      $sql = "Update orders set fin_attach = '".$data[0]. "',store_user = '".$data[1]."',store_isPacked = '".$data[2]."',o_state=3 Where o_id=".$order_no;

      //echo $sql;
      $res = self::$dbObject->query($sql);

      if($res->rowCount()>0)
         echo 'Confirmed' ;
      else
         print_r(['message'=>$sql  ,'status'=>false]);
   }*/

}

?>