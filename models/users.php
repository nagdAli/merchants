<?php

class Users extends Database{
    
    public function __construct(){}

    public function getUser($id){
        return self::$dbObject->query("select * from users WHERE u_isActive=1 and u_id=".$id)->fetchall(PDO::FETCH_ASSOC);
     }


     public function access($data=[]){
      $res = self::$dbObject->query('select * from users WHERE u_account="'.$data[0].'"')->fetch(PDO::FETCH_ASSOC);
     
      if(!$res)
         return false;
         
      if(password_verify($data[1],$res['u_pwd'])){
         $_SESSION['uid']=$res['u_id'];
         return true;
      }else{
         return false;
      }
   }


     public function getLastUserID(){
      return ((int)self::$dbObject->query("select u_id from users order by u_id desc")->fetch(PDO::FETCH_ASSOC)['u_id'])+1;
     }

     public function getLastStateID(){
      return ((int)self::$dbObject->query("select s_id from state order by s_id desc")->fetch(PDO::FETCH_ASSOC)['s_id'])+1;
     }

     public function getStates($user_id){
      return self::$dbObject->query('SELECT state.s_id as s_id,state.s_state as state,state.s_desc as state_desc,users.u_utype as j_title FROM state_access INNER JOIN state ON state_access.sa_id=state.s_id INNER JOIN users ON state_access.u_id = users.u_id WHERE state_access.u_id='.$user_id)->fetchall(PDO::FETCH_ASSOC);
     }



     public function changeStates($stateObjByUserID=null){
      if(!empty($stateObjByUserID)){
     
        $states = array_chunk(explode(',',$this->getStatesByJobTitle($stateObjByUserID)),4);

        $sel=[];
        foreach ($states as $k => $val) {
          if($states[$k][3]==1)
            array_push($sel,$states[$k][0]);
        }

        return self::$dbObject->query('select * from state where s_id IN('.implode(',',$sel).')')->fetchall(PDO::FETCH_ASSOC);
      }

    }


     protected function getStatesByJobTitle($uid){
      return self::$dbObject->query('select s_states from state_access where u_id='.$uid)->fetch(PDO::FETCH_ASSOC)['s_states'];
     } 


     public function perm($query){

      $sql='insert into perm()values(?,?)';

         //echo $sql;
         $res = self::$dbObject->prepare($sql);
         
         if($res->execute($query)){
            echo 'Created successfully';
         }else{
            var_dump($res->errorCode());
         }
         
     }


     public function addState($data){

      $sql='insert into state()values(?,?,?)';

         //echo $sql;
         $res = self::$dbObject->prepare($sql);
         
         if($res->execute($data)){
            echo 'Created successfully';
         }else{
            var_dump($res->errorCode());
         }
         
     }


     public function delState($stateID){

      $q = self::$dbObject->prepare("delete from state where s_id = ? ");

      $done = $q->execute([$stateID]);
      if($done){
         echo 'Deleted successfully';
      }else{
         echo 'Something goes wrong';
      }

    }

    public function updateState($sid, $states){

      $q = self::$dbObject->prepare("update state set s_state=? where s_id = ? ");

      $done = $q->execute([$states, $sid]);
      if($done){
         echo 'Updated successfully';
      }else{
         echo 'Something goes wrong';
      }

    }




     public function allAccounts(){
      return self::$dbObject->query("SELECT u_id,u_account FROM users order by u_id desc")->fetchall(PDO::FETCH_ASSOC);
   }

   public function getUsersByJobTitle($jtype){
      return self::$dbObject->query("SELECT u_id, u_fname,u_lname,u_cdate FROM users where u_utype=".$jtype)->fetchall(PDO::FETCH_ASSOC);
   }

   public function getStatedUsers($jtype){
      return self::$dbObject->query("SELECT u_id, u_fname,u_lname,u_cdate,is_stated FROM users where u_utype=".$jtype)->fetchall(PDO::FETCH_ASSOC);
   }
   

     public function searchByName($word){
        return self::$dbObject->query("SELECT u_id,u_account,u_cdate FROM users where u_account LIKE '".$word."%' order by u_id desc")->fetchall(PDO::FETCH_ASSOC);
     }
     
     public function searchByMGRName($word){
      return self::$dbObject->query("SELECT u_id,u_account,u_cdate FROM users where u_account LIKE '".$word."%' order by u_id desc")->fetchall(PDO::FETCH_ASSOC);
   }

     public function getEmpType(){
      return self::$dbObject->query('select * from empt_type')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getJobTitle($emp_type=null){
      if(!empty($emp_type))
      return self::$dbObject->query('select * from job_title where et_id='.$emp_type)->fetchall(PDO::FETCH_ASSOC);
      else
      return self::$dbObject->query('select * from job_title')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getCountery(){
      return self::$dbObject->query('select * from country')->fetchall(PDO::FETCH_ASSOC);
   }
   
   public function getMGRById($id){
      return self::$dbObject->query('select * from users where u_id='.$id)->fetchall(PDO::FETCH_ASSOC);
   }

   public function updateMGR($data=[]){
      $sql = "Update users set u_fname = '".$data['fname']. "',u_lname = '".$data['lname']."',u_gender = '".$data['gender']."',u_account = '".$data['u_account']."',u_mobile = '".$data['mobile']."',u_email = '".$data['email']."',u_country = '".$data['country']."',u_address = '".$data['maddress']."',u_utype = '".$data['u_utype']."',u_comment = '".$data['u_comment']."' Where u_id =" . $data['u_id'];
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم التعديل بنجاح';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }

   public function release($id){
      $sql = "Update users set u_status = 1 Where u_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم إلغاء الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }

   
   public function block($id){
      $sql = "Update users set u_status = 3 Where u_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }


   public static function update_pwd($id,$new_pwd,$old_pwd){
      $sql = "Select u_pwd from users where u_id = " . $id; 
      $res = self::$dbObject->query($sql);
      
      if($res->rowCount()>0)
      {
      //  if(password_verify($old_pwd,$res->fetch(PDO::FETCH_ASSOC)['u_pwd'])){
           $sql = "Update users set u_pwd = '".password_hash($new_pwd ,PASSWORD_BCRYPT)."' Where u_id =" . $id;
           $res = self::$dbObject->query($sql);
           if($res->rowCount()>0){
              //print_r(json_encode(['message'=>'Modified successfully.' .$sql,'status'=>true]));
              return true;
           }else{
               //print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
              return false;
           }
             
       /* }
        else 
         print_r(json_encode(['message'=>"old pwd not true",'status'=>false]));*/
          }
        else
           return false;
   }



}

?>