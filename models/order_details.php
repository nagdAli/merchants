<?php

class Order_details extends Database{
    
    public function __construct(){}

    public function getOrders($stateObjByUserID=null){
      if(empty($stateObjByUserID)){
      return self::$dbObject->query('select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id order by o_date limit 10')->fetchall(PDO::FETCH_ASSOC);
      }else{
        $states = array_chunk(explode(',',$this->getStatesByJobTitle($stateObjByUserID)),4);

        $sel=[];
        foreach ($states as $k => $val) {
          if($states[$k][2]==1)
            array_push($sel,$states[$k][0]);
        }

        return self::$dbObject->query('select *,state.s_state as state,state.s_id as s_id from orders INNER JOIN state ON orders.o_state=state.s_id where o_state IN('.implode(',',$sel).')')->fetchall(PDO::FETCH_ASSOC);
      }

    }
   

   protected function getStatesByJobTitle($uid){
    return self::$dbObject->query('select s_states from state_access where u_id='.$uid)->fetch(PDO::FETCH_ASSOC)['s_states'];
   } 

   public function getOrder($o_id){
    //return self::$dbObject->query('select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_id="'.$o_id.'"')->fetchall(PDO::FETCH_ASSOC);

    $res =  self::$dbObject->query('select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_id="'.$o_id.'"');
      if($res->rowCount()>0){
        return $res->fetchall(PDO::FETCH_ASSOC);
      }else
       return null; 
       
   }
   
   public function getNotEvaluatedOrders(){
    return self::$dbObject->query('select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where is_evaluated=0 order by o_date limit 10')->fetchall(PDO::FETCH_ASSOC);
 }


   public function getProductsByOrderId($o_id){
    return self::$dbObject->query('select * from products where o_id="'.$o_id.'"')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getDeliveredOrders(){
    return self::$dbObject->query('select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where orders.o_state = 5 order by o_date limit 20')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getOrdersForDelivery(){
    return self::$dbObject->query('select *,state.s_state as state,state.s_id as s_id from orders INNER JOIN state ON orders.o_state=state.s_id where o_state IN(3,4,5,6,7)')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getOrdersForFinance(){
    return self::$dbObject->query('select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_state IN(1,8)')->fetchall(PDO::FETCH_ASSOC);
   }

   public function getOrdersForStore(){
    return self::$dbObject->query('select *,state.s_state as state from orders INNER JOIN state ON orders.o_state=state.s_id where o_state=2')->fetchall(PDO::FETCH_ASSOC);
   }

    public static function getStates(){
        //var_dump(self::$dbObject);
        return ((!empty(self::$dbObject))?self::$dbObject->query("select * from state")->fetchall(PDO::FETCH_ASSOC):null); 
      } 
      
      public static function getStatesForCS(){
        //var_dump(self::$dbObject);
        return ((!empty(self::$dbObject))?self::$dbObject->query("select * from state where s_id=1 or s_id=10")->fetchall(PDO::FETCH_ASSOC):null); 
      } 

      public static function getPlatforms(){
        //var_dump(self::$dbObject);
        return ((!empty(self::$dbObject))?self::$dbObject->query("select * from platforms")->fetchall(PDO::FETCH_ASSOC):null); 
      } 

      public static function getPaymentType(){
         //var_dump(self::$dbObject);
         return ((!empty(self::$dbObject))?self::$dbObject->query("select * from pay_type")->fetchall(PDO::FETCH_ASSOC):null); 
       }  

       public static function getShipType(){
         //var_dump(self::$dbObject);
         return ((!empty(self::$dbObject))?self::$dbObject->query("select * from ship_type")->fetchall(PDO::FETCH_ASSOC):null); 
       }  

       public static function getDeliveryMethod(){
         //var_dump(self::$dbObject);
         return ((!empty(self::$dbObject))?self::$dbObject->query("select * from delivery_type")->fetchall(PDO::FETCH_ASSOC):null); 
       }  

       public static function cities(){
         //var_dump(self::$dbObject);
         return ((!empty(self::$dbObject))?self::$dbObject->query("select * from city")->fetchall(PDO::FETCH_ASSOC):null); 
       } 

       public static function productCategory(){
         //var_dump(self::$dbObject);
         return ((!empty(self::$dbObject))?self::$dbObject->query("select * from product_category")->fetchall(PDO::FETCH_ASSOC):null); 
       } 

       public static function cancel_reason(){
         //var_dump(self::$dbObject);
         return ((!empty(self::$dbObject))?self::$dbObject->query("select * from cancel_reason")->fetchall(PDO::FETCH_ASSOC):null); 
       }

      public function updateArea($data=[]){
        $sql = "Update area set area_name = '".$data[1]. "',area_latLang = '".$data[2]."',country = '".$data[3]."' Where area_id =" . $data[0];
        $res = self::$dbObject->query($sql);
        if($res->rowCount()>0)
           echo 'تم التعديل بنجاح';
        else
           print_r(['message'=>$res->errorCode(),'status'=>false]);
     }


}

?>