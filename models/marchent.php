<?php

class Marchent extends Database{
    
    public function __construct(){}

    public function getMarchentByID($id){
        return self::$dbObject->query("select * from marchent where m_id=".$id)->fetch(PDO::FETCH_ASSOC);
     }

     public function getMarchents(){
      return self::$dbObject->query("select * from marchent")->fetchall(PDO::FETCH_ASSOC);
   }


     public function getLastID(){
      return ((int)self::$dbObject->query("select m_id from marchent order by m_id desc limit 1")->fetch(PDO::FETCH_ASSOC)['m_id'])+1;
    }


    public function access($data=[]){
      $res = self::$dbObject->query('select * from marchent WHERE m_account="'.$data[0].'"')->fetch(PDO::FETCH_ASSOC);
     
      if(!$res)
         return false;
         
      if(password_verify($data[1],$res['m_pwd'])){
         $_SESSION['mid']=$res['m_id'];
         return true;
      }else{
         return false;
      }
   }



     public function perm($query){

      $sql='insert into perm()values(?,?)';

         //echo $sql;
         $res = self::$dbObject->prepare($sql);
         
         if($res->execute($query)){
            echo 'Created successfully';
         }else{
            var_dump($res->errorCode());
         }
         
     }


     public function getProducts($m){
      return self::$dbObject->query("select * from marchent where m_id=".$m)->fetchall(PDO::FETCH_ASSOC);
     }

     public function addState($data){

      $sql='insert into state()values(?,?,?)';

         //echo $sql;
         $res = self::$dbObject->prepare($sql);
         
         if($res->execute($data)){
            echo 'Created successfully';
         }else{
            var_dump($res->errorCode());
         }
         
     }


     public function delState($stateID){

      $q = self::$dbObject->prepare("delete from state where s_id = ? ");

      $done = $q->execute([$stateID]);
      if($done){
         echo 'Deleted successfully';
      }else{
         echo 'Something goes wrong';
      }

    }

    public function updateState($sid, $state){

      $q = self::$dbObject->prepare("update marchent set m_state=? where m_id = ? ");

      $done = $q->execute([$state,$sid ]);
      if($done){
         echo 'Updated successfully';
      }else{
         echo 'Something goes wrong';
      }

    }





   public function release($id){
      $sql = "Update users set u_status = 1 Where u_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم إلغاء الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }

   
   public function block($id){
      $sql = "Update users set u_status = 3 Where u_id =" .$id;
      $res = self::$dbObject->query($sql);
      if($res->rowCount()>0)
         echo 'تم الحظر';
      else
         print_r(['message'=>$res->errorCode(),'status'=>false]);
   }


   public static function update_pwd($id,$new_pwd,$old_pwd){
      $sql = "Select u_pwd from users where u_id = " . $id; 
      $res = self::$dbObject->query($sql);
      
      if($res->rowCount()>0)
      {
      //  if(password_verify($old_pwd,$res->fetch(PDO::FETCH_ASSOC)['u_pwd'])){
           $sql = "Update users set u_pwd = '".password_hash($new_pwd ,PASSWORD_BCRYPT)."' Where u_id =" . $id;
           $res = self::$dbObject->query($sql);
           if($res->rowCount()>0){
              //print_r(json_encode(['message'=>'Modified successfully.' .$sql,'status'=>true]));
              return true;
           }else{
               //print_r(json_encode(['message'=>$res->errorCode(),'status'=>false]));
              return false;
           }
             
       /* }
        else 
         print_r(json_encode(['message'=>"old pwd not true",'status'=>false]));*/
          }
        else
           return false;
   }



}

?>