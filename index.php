<?php
session_start();

require 'flight/Flight.php';
require_once 'auto_load.php';

Flight::set('flight.log_errors',true);
Flight::set('flight.views.path', './views');

Database::engage(new MySQL());

$m = new Marchent();
$user = new Users();


Flight::route('/reg', function(){

    Flight::render('home.php');

  });

  Flight::route('/', function(){

    Flight::render('index.php');

  });

  Flight::route('/kb', function(){

    Flight::render('user-guide.php');

  });

  Flight::route('/faqs', function(){

    Flight::render('common-questions.php');

  });


  Flight::route('/login', function(){

    Flight::render('login.php');

  });


  Flight::route('/logout', function(){

    session_destroy();
    Flight::redirect('/login');

  });

  Flight::route('admin/logout', function(){

    session_destroy();
    Flight::redirect('/admin');

  });


  Flight::route('/pendding', function()use($m){

    if(empty($_SESSION['mid'])){
    Flight::redirect('/login');
    }else{
      Flight::render('pendding.php',[
        'data'=>$m->getMarchentByID($_SESSION['mid'])
      ]);
    }

  });

  Flight::route('POST /admin/auth', function()use($m){
    $data = Flight::request()->data;

  if($m->access([$data['username'],$data['userpwd']])){
      echo '1';
   }else{
      echo '0';
   }
      
});





Flight::route('POST /data/upload', function()use($m){
     
  $target_dir = "./uploads/";

  $data = Flight::request()->data;    
    
  $img_list='';
  //if(!is_string($data['p_img'])){
 
    $countfiles = count($_FILES['spimgs']['name']);
 
    if($countfiles>0){
     for($i=0;$i<$countfiles;$i++){
       $new_img_name= 'baz_'.rand(1,999999);
       $file_name=explode('.',$_FILES['spimgs']['name'][$i]);
       $ext=end($file_name);
       $img_list.= rtrim($new_img_name.'.'.$ext,',').',';
       move_uploaded_file($_FILES['spimgs']['tmp_name'][$i],$target_dir.$new_img_name.'.'.$ext);
   
      }
     }else{
         $img_list='';
     }


       $new_doc_name= 'baz_'.rand(1,999999);
       $file_name1=explode('.',$_FILES['sdocfile']['name']);
       $ext1=end($file_name1);
       move_uploaded_file($_FILES['sdocfile']['tmp_name'],$target_dir.$new_doc_name.'.'.$ext1);


       $new_logo_name= 'baz_'.rand(1,999999);
       $file_name2=explode('.',$_FILES['slogopic']['name']);
       $ext2=end($file_name2);
       move_uploaded_file($_FILES['slogopic']['tmp_name'],$target_dir.$new_logo_name.'.'.$ext2);



       $m->uploadFiles(
        $new_doc_name.'.'.$ext1,
        rtrim($img_list,','),
        $new_logo_name.'.'.$ext2,
        $_SESSION['mid']
      );


});




  Flight::route('/new', function()use($m){

    $data = Flight::request()->data;

    $m->insert([
                $m->getLastID(),
                $data['sun'],
                $data['sfn'],
                password_hash($data['passwd'], PASSWORD_BCRYPT),
                $data['sfg'],
                $data['smn'],
                $data['sidno'],
                $data['sidc'],
                '',
                $data['spw'],
                $data['scl'],
                '',            //products images
                1,
                $data['sd'],
                $data['sa'],
                $data['snb'],
                $data['ssn'],
                0,
                ''
    ]);

  });


 


  /*
     Dashboard section
  */

  Flight::route('/admin', function(){

    Flight::render('dashboard_login.php');

  });

  Flight::route('/admin/dashboard', function()use($m){

    Flight::render('dashboard.php',[
      'list'=>$m->getMarchents()
    ]);

  });

  Flight::route('/admin/seller/@id', function($id)use($m){

    Flight::render('view-seller.php',[
      'products'=>$m->getProducts($id)
    ]);

  });


  Flight::route('POST /admin/check', function()use($user){
    $data = Flight::request()->data;

  if($user->access([$data['username'],$data['userpwd']])){
      echo '1';
   }else{
      echo '0';
   }
      
});

Flight::route('POST /admin/state', function()use($m){
  $data = Flight::request()->data;
  //var_dump($data);
  switch ($data['state']) {
    case '3':
      $m->updateState($data['sid'],3);
      break;
      case '4':
        $m->updateState($data['sid'],4);
        break;
    default:
      return false;
  }
    
});



Flight::start();
