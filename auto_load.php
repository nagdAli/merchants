<?php

global $config;
$config = require_once 'core/env.php';

require_once 'app/DB.php';

$fileList = glob('models/*');
 
foreach($fileList as $filename){
   require_once $filename; 
}

?>