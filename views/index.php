<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="views/css/knowledge.css">
    <link rel="stylesheet" href="views/css/fontello.css">
    <link rel="stylesheet" href="views/css/animation.css">
    <script src="views/js/jquery-3.3.1.min.js"></script>
    <script src="views/js/knowledge.js"></script>
    <title>Bazzarry - Seller</title>
</head>
<body>
    <header class="fixed">
        <nav class="main-nav">
            <a href="#" class="nav-logo">
                <img src="views/img/logo.webp" alt="Bazzarry Logo">
            </a>
            <ul class="nav-menu">
                <li class="nav-menu-item">
                    <a href="#join-us" class="menu-item-content active-tab">
                        انضم لنا
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="./" class="menu-item-content">
                        لماذا بازاري
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="#contacts" class="menu-item-content">
                        تواصل معنا
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="./kb" class="menu-item-content">
                        قاعدة المعرفة
                    </a>
                </li>
            </ul>
            <button type="button" id="menu-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </nav>
    </header>
    <section class="full intro" id="join-us">
        <div class="skewed">
            <h1>التجارة الألكترونية</h1>
            <h3>بطريقة لم تتخليها من قبل</h3>
            <p>نص لا يزيد عن سطرين هنا لوصف التجربة نص لا يزيد عن سطرين هنا لوصف التجربة</p>
            <button class="pr-btn">انضم لنا</button>
            <img src="views/img/lines.png" alt="dashes" class="intro-drcoration">
        </div>
    </section>
    <section class="full features" id="features">
        <div class="section-title">
            <h3 class="has-decoration">لماذا بازاري</h3>
        </div>
        <div class="features-container">
            <div class="feature">
                <i class="icon-globe"></i>
                <h4>عنوان الميزة</h4>
                <p>يوجد هنا نص لوصف الميزة وهذه المساحة لمعاينة التصميم وأبعاده فقط</p>
            </div>
            <div class="feature">
                <i class="icon-globe"></i>
                <h4>عنوان الميزة</h4>
                <p>يوجد هنا نص لوصف الميزة وهذه المساحة لمعاينة التصميم وأبعاده فقط</p>
            </div>
            <div class="feature">
                <i class="icon-globe"></i>
                <h4>عنوان الميزة</h4>
                <p>يوجد هنا نص لوصف الميزة وهذه المساحة لمعاينة التصميم وأبعاده فقط</p>
            </div>
        </div>
    </section>
    <section class="full contact-us" id="contacts">
        <div class="contact-sections">
            <section class="right">
                <h3 class="has-decoration">تواصل معنا</h3>
                <p>يوجد هنا نص لا يزيد عن سطرين يصف العنوان والغرض منه هو شغر مساحة لمعاينة التصميم وأبعاده فقط</p>
                <div class="contact-form">
                    <div class="control">
                        <label class="control-label">
                            الاسم الثلاثي
                            <span class="required-field">*</span>
                        </label>
                        <input type="text" class="control-input" id="cname">
                    </div>
                    <div class="control">
                        <label class="control-label">
                            البريد الالكتروني
                            <span class="required-field">*</span>
                        </label>
                        <input type="email" class="control-input" id="cemail">
                    </div>
                    <div class="control">
                        <label class="control-label">
                             الرسالة 
                            <span class="required-field">*</span>
                        </label>
                        <textarea class="control-input" id="cmsg"></textarea>
                    </div>
                    <div class="contact-action">
                        <button class="pr-btn" id="send-msg">
                            <i class="icon-paper-plane"></i>
                            <span>ارسال</span>
                        </button>
                        <span class="contact-err">تأكد من ملئ كافة البيانات</span>
                    </div>
                </div>
            </section>
            <section class="left">
                <div class="contact-img">
                    <img src="views/img/contact-ways.png" alt="Contact Ways Picture">
                </div>
                <div class="contact-info">
                    <p>8000501 <i class="icon-phone"></i></p>
                    <p>wecare@bazzarry.com <i class="icon-mail-1"></i></p>
                    <p>facebookpage@<i class="icon-facebook"></i></p>
                    <p>twitteraccount@<i class="icon-twitter"></i></p>
                    <p>youtubeaccount@<i class="icon-youtube"></i></p>
                </div>
            </section>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            window.location.href = "#join-us";
        });
    </script>
</body>
</html>