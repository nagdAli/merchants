$(document).ready(function(){
    $("#slog-btn").click(function(){
        if($("#sun").val().trim() != "" && $("#spw").val().trim() != "")
        {
            var username = $("#sun").val().trim();
            var userpwd = $("#spw").val().trim();
            $.ajax({
                url: "./admin/check",
                type: "POST",
                data: {
                    username:username,
                    userpwd:userpwd
                },
                success: function(data){
                    //alert(data)
                    if(data==="1")
                      window.location.href='./admin/dashboard'
                    else
                    alert(data);
                }
            });
            
        }
        else
        {
            $(".err-msg").fadeIn(400).delay(10000).fadeOut(400);
        }
    });

    $(".mini-approve").click(function(){
        var sid = $(this).parent().attr("sid");
        //alert(sid);
       $.ajax({
            url: "../admin/state",
            type: "POST",
            data: {
                sid:sid,
                state:3
            },
            success: function(data){
                alert(data)
               
            }
        });
    }); 

    $(".mini-discard").click(function(){
        var sid = $(this).parent().attr("sid");

        //alert(sid);
        $.ajax({
            url: "../admin/state",
            type: "POST",
            data: {
                sid:sid,
                state:4
            },
            success: function(data){
                alert(data)
               
            }
        });
    }); 
});