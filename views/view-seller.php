<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../views/css/dashboard.css">
    <link rel="stylesheet" href="../../views/css/fontello.css">
    <link rel="stylesheet" href="../../views/css/animation.css">
   
    <title>Admin Dashboard</title>
</head> 
<body>
    <div class="page-wrapper">
        <div class="page-content">
            <aside class="side-menu" id="admin-menu">
                <header class="menu-header">
                    <img src="../../views/img/logo.webp" alt="Bazzarry logo">
                </header>
                <ul class="side-menu-items"> 
                    <li>
                        <a href="#">
                            <i class="icon-gauge-1"></i>
                            <span>لوحة التحكم</span>
                        </a>
                    </li>
                    <li>
                        <a href="../../admin">
                            <i class="icon-logout"></i>
                            <span>تسجيل الخروج</span>
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="content-section"> 
                <section class="row-section">
                    <h3 class="section-title">بيانات التاجر</h3>
                    <div class="seller-intro">
                        <img src="../../uploads/<?= $products[0]['m_logo']; ?>" alt="Store logo">
                        <div class="seller-main-data">
                            <h3><?= $products[0]['m_store_title']; ?></h3>
                            <h5><?= $products[0]['m_full_name']; ?></h5>
                        </div>
                    </div>
                </section>
                <section class="row-section">
                    <h3 class="section-title">بيانات التواصل</h3>
                    <div class="key-value">
                        <span class="key">رقم الجوال</span>
                        <span class="value"><?= $products[0]['m_mobile']; ?></span>
                    </div>
                   <!-- <div class="key-value">
                        <span class="key">رقم الهاتف</span>
                        <span class="value">02-287410</span>
                    </div>-->
                </section>
                <section class="row-section">
                    <h3 class="section-title">بيانات العنوان</h3>
                    <div class="key-value">
                        <span class="key">الدولة</span>
                        <span class="value">اليمن</span>
                    </div>
                    <div class="key-value">
                        <span class="key">المنطقة</span>
                        <span class="value">عدن</span>
                    </div>
                    <div class="key-value">
                        <span class="key">العنوان</span>
                        <span class="value"><?= $products[0]['m_address']; ?></span>
                    </div>
                    <div class="key-value">
                        <span class="key">أقرب معلم</span>
                        <span class="value"><?= $products[0]['m_next_to']; ?></span>
                    </div>
                </section>
                <section class="row-section">
                    <h3 class="section-title">بيانات طبيعة العمل</h3>
                    <div class="key-value">
                        <span class="key">نوع النشاط</span>
                        <span class="value">شخصي</span>
                    </div>
                    <div class="key-value">
                        <span class="key">نوع الهوية</span> 
                        <span class="value">بطاقة شخصية</span>
                    </div>
                    <div class="key-value">
                        <span class="key">رقم الهوية</span>
                        <span class="value"><?= $products[0]['m_cart']; ?></span>
                    </div>
                    <div class="img-preview">
                        <span>صورة الهوية</span>
                        <img src="../../uploads/<?= $products[0]['m_cart_file']; ?>" alt="Seller Identity Picture">
                    </div>
                </section>
                <section class="row-section">
                    <h3 class="section-title">بيانات  المتجر</h3>
                    <div class="key-value">
                        <span class="key">اسم المتجر</span>
                        <span class="value"><?= $products[0]['m_store_title']; ?></span>
                    </div>
                    <div class="img-preview">
                        <span>شعار المتجر</span>
                        <img src="../../uploads/<?= $products[0]['m_logo']; ?>" alt="Seller Identity Picture">
                    </div>
                    <div class="img-preview">
                        <span>موقع المتجر</span>
                        <div id="map-location"><?= $products[0]['m_location']; ?></div>
                    </div>
                    <div class="img-preview multiple">
                        <span>صور المنتجات</span>
                        
                        <?php

                           foreach ($products as $k => $val) {

                               if(!empty(explode(',',$val['m_p_imgs']))){

                                    foreach (explode(',',$val['m_p_imgs']) as $product) {
                                        echo '<img src="../../uploads/'.$product.'" alt="Seller Identity Picture">';
                                    }

                            }else{
                                echo '<img src="../../uploads/'.$val['m_p_imgs'].'" alt="Seller Identity Picture">';
                            }

                           }

                        ?>
                    </div>
                </section>
                <div class="options">
                    <button class="mini-approve">
                        <i class="icon-ok"></i>
                        <span>قبول</span>
                    </button>
                    <button class="mini-discard">
                        <i class="icon-cancel"></i>
                        <span>رفض</span>
                    </button>
                </div>
            </section>
        </div>
    </div>
    <script src="../../views/js/jquery-3.3.1.min.js"></script>
    <script src="../../views/js/dashboard.js"></script>
</body>
</html>