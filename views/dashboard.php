<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/css/dashboard.css">
    <link rel="stylesheet" href="../views/css/fontello.css">
    <link rel="stylesheet" href="../views/css/animation.css">
    <script src="../views/js/jquery-3.3.1.min.js"></script>
    <script src="../views/js/dashboard.js"></script>
    <title>Admin Dashboard</title>
</head> 
<body>
    <div class="page-wrapper">
        <div class="page-content">
            <aside class="side-menu" id="admin-menu">
                <header class="menu-header">
                    <img src="../views/img/logo.webp" alt="Bazzarry logo">
                </header>
                <ul class="side-menu-items"> 
                    <li>
                        <a href="#">
                            <i class="icon-gauge-1"></i>
                            <span>لوحة التحكم</span>
                        </a>
                    </li>
                    <li>
                        <a href="../admin">
                            <i class="icon-logout"></i>
                            <span>تسجيل الخروج</span>
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="content-section"> 
                <section class="row-section">
                    <h3 class="section-title">الاحصائيات والمؤشرات</h3>
                    <section class="cards-container">   
                        <div class="statistic-card">
                            <h5 class="card-title">طلبات اليوم</h5>
                            <h2 class="card-value">20</h2>
                            <footer class="card-tail">
                                <div class="detailed-statistics">
                                    <span class="detail-title">المعلقة</span>
                                    <div class="circular-progress">
                                        <h5>14</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المقبولة</span>
                                    <div class="circular-progress">
                                        <h5>4</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المرفوضة</span>
                                    <div class="circular-progress">
                                        <h5>2</h5>
                                    </div>
                                </div>
                            </footer>
                        </div>
                        <div class="statistic-card">
                            <h5 class="card-title">طلبات الانضمام</h5>
                            <h2 class="card-value">20</h2>
                            <footer class="card-tail">
                                <div class="detailed-statistics">
                                    <span class="detail-title">المعلقة</span>
                                    <div class="circular-progress">
                                        <h5>14</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المقبولة</span>
                                    <div class="circular-progress">
                                        <h5>4</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المرفوضة</span>
                                    <div class="circular-progress">
                                        <h5>2</h5>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </section>
                </section>
                <section class="row-section">
                    <h3 class="section-title">طلبات الانضمام الحديثة</h3>
                    <section class="listed-data-rows">
                        <ul class="list-row"> 

                        <?php
                         foreach ($list as $k => $val) {
                            if($val['m_state']!=4){
                         echo '<li>
                             <div class="data">
                                 <img src="../uploads/'.$val['m_logo'].'" alt="store logo">
                                 <span>'.$val['m_store_title'].'</span>
                                 <span>'.$val['m_full_name'].'</span>
                             </div>
                             <div class="options" sid="'.$val['m_id'].'">
                                <span>الحالة : '.($val['m_state']==1?"غير مكتمل":($val['m_state']==2?"معلق":($val['m_state']==3?"مقبول":"مرفوض"))).'</span>
                                <a href="../admin/seller/'.$val['m_id'].'" class="mini-preview">
                                    <i class="icon-eye"></i>
                                    <span>عرض</span>
                                </a>';
                          if($val['m_state']!=3){
                                echo'<button class="mini-approve">
                                    <i class="icon-ok"></i>
                                    <span>قبول</span>
                                </button>
                                <button class="mini-discard">
                                    <i class="icon-cancel"></i>
                                    <span>رفض</span>
                                </button>
                             </div>';
                          }
                     echo'</li>';
                            }
                     
                    }
                         ?>
                            
                         
                        </ul>
                    </section>
                </section>
            </section>
        </div>
    </div>
</body>
</html>