<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/frontend.js"></script>
    <title>Admin Dashboard</title>
</head> 
<body>
    <div class="page-wrapper">
        <div class="page-content">
            <aside class="side-menu" id="admin-menu">
                <header class="menu-header">
                    <img src="img/logo.webp" alt="Bazzarry logo">
                </header>
                <ul class="side-menu-items"> 
                    <li>
                        <a href="#">
                            <i class="icon-gauge-1"></i>
                            <span>لوحة التحكم</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-logout"></i>
                            <span>تسجيل الخروج</span>
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="content-section"> 
                <section class="row-section">
                    <h3 class="section-title">بيانات التاجر</h3>
                    <div class="seller-intro">
                        <img src="img/user1.jpg" alt="Store logo">
                        <div class="seller-main-data">
                            <h3>اسم المحل هنا</h3>
                            <h5>اسم التاجر هنا</h5>
                        </div>
                    </div>
                </section>
                <section class="row-section">
                    <h3 class="section-title">بيانات التواصل</h3>
                    <div class="key-value">
                        <span class="key">رقم الجوال</span>
                        <span class="value">736420152</span>
                    </div>
                    <div class="key-value">
                        <span class="key">رقم الهاتف</span>
                        <span class="value">02-287410</span>
                    </div>
                </section>
                <section class="row-section">
                    <h3 class="section-title">بيانات العنوان</h3>
                    <div class="key-value">
                        <span class="key">الدولة</span>
                        <span class="value">اليمن</span>
                    </div>
                    <div class="key-value">
                        <span class="key">المنطقة</span>
                        <span class="value">عدن</span>
                    </div>
                    <div class="key-value">
                        <span class="key">العنوان</span>
                        <span class="value">كريتر - السوق</span>
                    </div>
                    <div class="key-value">
                        <span class="key">أقرب معلم</span>
                        <span class="value">السوق المركزي</span>
                    </div>
                </section>
                <section class="row-section">
                    <h3 class="section-title">بيانات طبيعة العمل</h3>
                    <div class="key-value">
                        <span class="key">نوع النشاط</span>
                        <span class="value">شخصي</span>
                    </div>
                    <div class="key-value">
                        <span class="key">نوع الهوية</span>
                        <span class="value">بطاقة شخصية</span>
                    </div>
                    <div class="key-value">
                        <span class="key">رقم الهوية</span>
                        <span class="value">0009752146</span>
                    </div>
                    <div class="img-preview">
                        <span>صورة الهوية</span>
                        <img src="img/image-icon.png" alt="Seller Identity Picture">
                    </div>
                </section>
            </section>
        </div>
    </div>
</body>
</html>