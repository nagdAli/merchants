<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/frontend.js"></script>
    <title>Admin Dashboard</title>
</head> 
<body>
    <div class="page-wrapper">
        <div class="page-content">
            <aside class="side-menu" id="admin-menu">
                <header class="menu-header">
                    <img src="img/logo.webp" alt="Bazzarry logo">
                </header>
                <ul class="side-menu-items"> 
                    <li>
                        <a href="#">
                            <i class="icon-gauge-1"></i>
                            <span>لوحة التحكم</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-logout"></i>
                            <span>تسجيل الخروج</span>
                        </a>
                    </li>
                </ul>
            </aside>
            <section class="content-section"> 
                <section class="row-section">
                    <h3 class="section-title">الاحصائيات والمؤشرات</h3>
                    <section class="cards-container">   
                        <div class="statistic-card">
                            <h5 class="card-title">طلبات اليوم</h5>
                            <h2 class="card-value">20</h2>
                            <footer class="card-tail">
                                <div class="detailed-statistics">
                                    <span class="detail-title">المعلقة</span>
                                    <div class="circular-progress">
                                        <h5>14</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المقبولة</span>
                                    <div class="circular-progress">
                                        <h5>4</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المرفوضة</span>
                                    <div class="circular-progress">
                                        <h5>2</h5>
                                    </div>
                                </div>
                            </footer>
                        </div>
                        <div class="statistic-card">
                            <h5 class="card-title">طلبات الانضمام</h5>
                            <h2 class="card-value">20</h2>
                            <footer class="card-tail">
                                <div class="detailed-statistics">
                                    <span class="detail-title">المعلقة</span>
                                    <div class="circular-progress">
                                        <h5>14</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المقبولة</span>
                                    <div class="circular-progress">
                                        <h5>4</h5>
                                    </div>
                                </div>
                                <div class="detailed-statistics">
                                    <span class="detail-title">المرفوضة</span>
                                    <div class="circular-progress">
                                        <h5>2</h5>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </section>
                </section>
                <section class="row-section">
                    <h3 class="section-title">طلبات الانضمام الحديثة</h3>
                    <section class="listed-data-rows">
                        <ul class="list-row"> 
                            <li>
                                <a href="#">
                                    <div class="data">
                                        <img src="img/user1.jpg" alt="store logo">
                                        <span>اسم المحل هنا</span>
                                        <span>اسم التاجر هنا</span>
                                    </div>
                                    <div class="options">
                                        <button class="mini-approve">
                                            <i class="icon-ok"></i>
                                            <span>قبول</span>
                                        </button>
                                        <button class="mini-discard">
                                            <i class="icon-cancel"></i>
                                            <span>رفض</span>
                                        </button>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="data">
                                        <img src="img/user2.jpg" alt="store logo">
                                        <span>اسم المحل هنا</span>
                                        <span>اسم التاجر هنا</span>
                                    </div>
                                    <div class="options">
                                        <button class="mini-approve">
                                            <i class="icon-ok"></i>
                                            <span>قبول</span>
                                        </button>
                                        <button class="mini-discard">
                                            <i class="icon-cancel"></i>
                                            <span>رفض</span>
                                        </button>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="data">
                                        <img src="img/user3.jpg" alt="store logo">
                                        <span>اسم المحل هنا</span>
                                        <span>اسم التاجر هنا</span>
                                    </div>
                                    <div class="options">
                                        <button class="mini-approve">
                                            <i class="icon-ok"></i>
                                            <span>قبول</span>
                                        </button>
                                        <button class="mini-discard">
                                            <i class="icon-cancel"></i>
                                            <span>رفض</span>
                                        </button>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </section>
                </section>
            </section>
        </div>
    </div>
</body>
</html>