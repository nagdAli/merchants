<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="views/css/style.css">
    <title>Bazzarry - Pending</title>
</head>
<body>
    <div id="pendding-page-wrapper">
        <img src="views/img/logo.webp" alt="Website Logo">
        <h1>عزيزي التاجر اسم التاجر</h1>
        <?php
         if($data['m_state']==1){
        ?>
       
       <?php
         }
       ?>

        <?php
         if($data['m_state']==2){
        ?>
        

        <?php
         }
       ?>        

       <?php
         switch ($data['m_state']) {
             case '1':
                echo ' <p>لقد استكملت مرحلة إضافة البيانات وتبقى رفع الملفات والوثائق المطلوبة لإنهاء عملية التسجيل</p>';
                 break;
                 case '2':
                   echo '<p>لقد استكملت مرحلة التسجيل كتاجر ويتم مراجعة البيانات التي قمت بإضافتها حاليًا وسيتم التواصل معك قريبًا.</p>';
                    break;
                    case '3':
                        echo ' <p>عزيزي التاجر لقد تم قبول طلبك</p>';
                        break;
                        case '4':
                            echo ' <p>نعتذر لك عزيزي التاجر, لقد تم رفض طلبك</p>';
                            break;
             default:
                return false;
                 break;
         }
       ?>
       <a href="./logout" style="padding:5px 15px;background-color:var(--placeholder-color);margin-top:20px;">تسجيل الخروج</a>
    </div>
    <?php
     if($data['m_state']==1){
    ?>
    <form id="surf" action="./data/upload" method="POST" enctype="multipart/form-data">
        <div class="controls-container">
            <p>الحقول التي تحتوي على العلامة (*) هي حقول إلزامية</p>
            <button type="button" class="upload-btn" id="usdi-btn">
                + إضافة صورة المستند
                <span>*</span>
            </button>
            <input type="file" id="sdocfile" accept=".jpg, .jpeg, .png , .gif , .tiff " name="sdocfile">
            <div class="udinfo">
                <img src="img/image-icon.png" alt="image icon">
                <span id="sdocpic"></span>
            </div>
            <button type="button" class="upload-btn" id="uslogo-btn">
                + إضافة شعار المتجر
                <span>*</span>
            </button>
            <input type="file" class="ftslide-input" id="slogopic" accept=".jpg, .jpeg, .png , .gif , .tiff " name="slogopic">
            <div class="udinfo">
                <img src="img/image-icon.png" alt="image icon">
                <span id="splogopicn"></span>
            </div>
            <button type="button" class="upload-btn" id="uspi-btn">
                + إضافة صور المنتجات
                <span>(10 منتجات كحد أقصى) *</span> 
            </button>
            <input type="file" class="ftslide-input" id="spimgs" accept=".jpg, .jpeg, .png , .gif , .tiff " name="spimgs[]" multiple>
            <div class="udinfo">
                <img src="img/image-icon.png" alt="image icon">
                <span id="sppics"></span>
            </div>
            <div id="btn-options">
                <button type="submit" id="sufbtn">رفع الملفات</button>
                <button type="button" id="signout">الاستكمال لاحقًا</button>
            </div>
        </div>
    </form>
    <?php
     }
    ?>

    <script src="views/js/jquery-3.3.1.min.js"></script>
    <script src="views/js/frontend.js"></script>
</body>
</html>