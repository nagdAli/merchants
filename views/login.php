<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="views/css/style.css">
    <title>Bazzarry - Seller Login</title>
</head>
<body>
    <div id="login-page-wrapper">
        <header>
            <img src="views/img/logo.webp" alt="Bazzarry logo">
            <h1>سجل دخولك لحساب التاجر الخاص بك</h1>
            <p>ليس لديك حساب مسبق؟</p>
            <a href="./login">قم بانشاء حساب</a>
        </header>
        <div id="login-box">
            <div class="controls-container">
                <p>الحقول التي تحتوي على العلامة (*) هي حقول إلزامية</p>
                <div class="control">
                    <label class="control-label">
                        اسم المستخدم 
                        <span class="required-field">*</span>
                    </label>
                    <input type="text" class="control-input" id="sun">
                </div>
                <div class="control">
                    <label class="control-label">
                        كلمة المرور 
                        <span class="required-field">*</span>
                    </label>
                    <input type="password" class="control-input" id="spw">
                </div>
                <button id="slog-btn">تسجيل الدخول</button>
                <p class="err-msg">يُرجى التأكد من ملئ كافة البيانات</p>
            </div>
        </div>
    </div>
    <script src="views/js/jquery-3.3.1.min.js"></script>
    <script src="views/js/frontend.js"></script>
</body>
</html>