<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="views/css/knowledge.css">
    <link rel="stylesheet" href="views/css/fontello.css">
    <link rel="stylesheet" href="views/css/animation.css">
    <script src="views/js/jquery-3.3.1.min.js"></script>
    <script src="views/js/knowledge.js"></script>
    <title>Bazzarry - Seller</title>
</head>
<body>
    <header class="fixed">
        <nav class="main-nav">
            <a href="#" class="nav-logo">
                <img src="views/img/logo.webp" alt="Bazzarry Logo">
            </a>
            <ul class="nav-menu">
                <li class="nav-menu-item">
                    <a href="./" class="menu-item-content">
                        الرئيسية 
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="#knowledge-base" class="menu-item-content active-tab">
                        قاعدة المعرفة
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="./faqs" class="menu-item-content">
                        الأسئلة الشائعة
                    </a>
                </li>
            </ul>
            <button type="button" id="menu-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </nav>
    </header>
    <section class="full knowledge" id="knowledge-base">
        <h3 class="has-decoration">قاعدة المعرفة</h3>
        <p class="section-description"> يوجد هنا نص لوصف قسم قاعدة المعرفة لا يزيد عن سطرين يوجد هنا نص لوصف قسم قاعدة المعرفة لا يزيد عن سطرين</p>
        <div class="tabs">
            <div class="tab">
                <header>
                    <i class="icon-plus"></i>
                    <h4>نص اعتباطي لمعاينة مساحة السؤال</h4>
                </header>
                <div class="answer">
                    <p>نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة</p>
                </div>
            </div>
            <div class="tab">
                <header>
                    <i class="icon-plus"></i>
                    <h4>نص اعتباطي لمعاينة مساحة السؤال</h4>
                </header>
                <div class="answer">
                    <p>نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة</p>
                </div>
            </div>
            <div class="tab">
                <header>
                    <i class="icon-plus"></i>
                    <h4>نص اعتباطي لمعاينة مساحة السؤال</h4>
                </header>
                <div class="answer">
                    <p>نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة</p>
                </div>
            </div>
            <div class="tab">
                <header>
                    <i class="icon-plus"></i>
                    <h4>نص اعتباطي لمعاينة مساحة السؤال</h4>
                </header>
                <div class="answer">
                    <p>نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة</p>
                </div>
            </div>
            <div class="tab">
                <header>
                    <i class="icon-plus"></i>
                    <h4>نص اعتباطي لمعاينة مساحة السؤال</h4>
                </header>
                <div class="answer">
                    <p>نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة</p>
                </div>
            </div>
            <div class="tab">
                <header>
                    <i class="icon-plus"></i>
                    <h4>نص اعتباطي لمعاينة مساحة السؤال</h4>
                </header>
                <div class="answer">
                    <p>نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة نص اعتباطي لمعاينة مساحة الإجابة</p>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            window.location.href = "#knowledge-base";
        });
    </script>
</body>
</html>